package pjs;

import skills.Spells;

public abstract class PJ implements java.io.Serializable {
	/**
	 *  serialVersionUID = 3023332479937977557L
	 */
	private static final long serialVersionUID = 3023332479937977557L;
	protected boolean PlayerCharacter;        	//Dictamina si el jugador esta controlando este personaje,
				// STATS COMBATE ACTIVO
	protected int HP; 				       		// Vida Max
	protected int HPLive; 			       		// Vida actual
	protected int Mana; 						// Mana Max
	protected int ManaLive; 		       		//Mana actual
	protected int Initiative; 		       		//float Speed; Quiero cambiarlo a un sistema de segundos/ticks y no de turnos
	
				// INVENTARIOS
	protected int ISlots; 			       		// Numero de slots del inventario
	protected int Gold; 			       		// Dineritos
	protected item.Inventory Inventory;			// Un objeto "Invetario" estaria mejor
	protected Spells[] Spells;		       		// A saber...
	
				// DMG
	protected float CritPercent; 				// % Critico
	protected float CritMult; 					// +% de daño critico 
	protected int MinDMG; 						
	protected int MaxDMG; 						
	
				// DEFENSA 
	protected int Armor;						// Determina el % de defensa fisica
	protected int MagicalDef;					// Determina el % de defensa magica
	protected float Dodge; 						// % de esquivar
	protected float Parry; 						// % de Parry
	
				// Stats Personaje	 
	protected int Level; 						// Nivel ACTUAL
	protected long EXP;							// Experiencia actual
	protected long EXPNext;						// Experiencia para el siguiente nivel ExpToNextLevel - EXP
	protected long EXPToNextLevel;				// Experiencia necesaria para el siguiente nivel, SE ACUMULA INDEFINIDAMENTE
	
				// Attributes
	protected int Stamina;						// VIDA
	protected int Agility;						// Daño crit, iniciativa
	protected int Strengh;						// Daño fisico y main stat Warrior
	protected int Intelligence;					// Daño spells y mana maximo
	protected int Luck;							// Suerte de loot crit? No se si usarlo
	protected int Spirit;						// Regeneracion de MANA
	protected int[] Stats;						// Determina los stats primarios, secundarios y terciarios y luck (Al final)
	
	public void LvLUP() {
		this.EXPToNextLevel = util.UtilsMain.ExpLvL(this.Level+1);
		this.Level += 1;
	}
	
}
