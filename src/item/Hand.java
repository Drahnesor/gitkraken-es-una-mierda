package item;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public abstract class Hand extends Wearable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6305523893357328206L;
	protected boolean TwoHanded; 	// Es de dos manos?
	protected boolean SecondHand; 	// Es un objeto de segunda mano?
	
}
