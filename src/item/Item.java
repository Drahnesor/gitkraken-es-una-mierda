package item;

public abstract class Item implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1385973991702637391L;
	protected int IDItem; //
	protected String Name;
	protected String Description; 
	protected int Value; // En oro GOLD
	protected int IDCopia; // Para guardar las copias de los items, ya que serian random (GRINDEAR PERFECTOOOOS)
	protected int Rareza;
	
	public int getIDItem() {
		return IDItem;
	}
	public void setIDItem(int iDItem) {
		IDItem = iDItem;
	}
	public String getName() {
		return Name;
	}
	public void setNome(String name) {
		Name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public int getValue() {
		return Value;
	}
	public void setValue(int value) {
		Value = value;
	}
}
