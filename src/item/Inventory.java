package item;

public class Inventory implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7770029170541730381L;
	
	private item.Weapon FirstHand;
	private item.Hand SecondHand;
	private armor.Chest Chest;
	private armor.Boots Boots;
	private armor.Helmet Helmet;
	private armor.Necklace Necklace;
	private armor.Ring Ring1;
	private armor.Ring Ring2;
	private armor.Pants Pants;
	private armor.Shoulders Shoulders;
	private armor.Cape Cape;
	private Item[] Inventory;
	
	public Inventory() {
		 FirstHand = new hand.Sword();
		 SecondHand = new hand.Shield(); 
		 Chest = new armor.Chest();
		 Boots = new armor.Boots();
		 Helmet = new armor.Helmet();
		 Necklace = new armor.Necklace();
		 Ring1 = new armor.Ring();
		 Ring2 = new armor.Ring();
		 Pants = new armor.Pants();
		 Shoulders = new armor.Shoulders();
		 Cape = new armor.Cape();
		 setInventory(new Item[50]);
	}
	
	public void printInventoryEQ() {
		System.out.println("Arma: "+FirstHand.getName());
		System.out.println("Segunda Mano: "+SecondHand.getName());
		System.out.println("Pechera: "+Chest.getName());
		System.out.println("Botas: "+Boots.getName());
		System.out.println("Casco: "+Helmet.getName());
		System.out.println("Collar: "+Necklace.getName());
		System.out.println("Anillo Derecho: "+Ring1.getName());
		System.out.println("Anillo Izquierdo: "+Ring2.getName());
		System.out.println("Pantalones: "+Pants.getName());
	}
	
	public armor.Ring getRing2() {
		return Ring2;
	}
	public void setRing2(armor.Ring ring2) {
		Ring2 = ring2;
	}
	public armor.Pants getPants() {
		return Pants;
	}
	public void setPants(armor.Pants pants) {
		Pants = pants;
	}
	public armor.Ring getRing1() {
		return Ring1;
	}
	public void setRing1(armor.Ring ring1) {
		Ring1 = ring1;
	}
	public armor.Necklace getNecklace() {
		return Necklace;
	}
	public void setNecklace(armor.Necklace necklace) {
		Necklace = necklace;
	}
	public armor.Helmet getHelmet() {
		return Helmet;
	}
	public void setHelmet(armor.Helmet helmet) {
		Helmet = helmet;
	}
	public armor.Boots getBoots() {
		return Boots;
	}
	public void setBoots(armor.Boots boots) {
		Boots = boots;
	}
	public armor.Chest getChest() {
		return Chest;
	}
	public void setChest(armor.Chest chest) {
		Chest = chest;
	}
	public item.Weapon getFirstHand() {
		return FirstHand;
	}
	public void setFirstHand(item.Weapon firstHand) {
		FirstHand = firstHand;
	}
	public item.Hand getSecondHand() {
		return SecondHand;
	}
	public void setSecondHand(item.Hand secondHand) {
		SecondHand = secondHand;
	}

	public Item[] getInventory() {
		return Inventory;
	}

	public void setInventory(Item[] inventory) {
		Inventory = inventory;
	}

	public armor.Shoulders getShoulders() {
		return Shoulders;
	}

	public void setShoulders(armor.Shoulders shoulders) {
		Shoulders = shoulders;
	}

	public armor.Cape getCape() {
		return Cape;
	}

	public void setCape(armor.Cape cape) {
		Cape = cape;
	}

}
