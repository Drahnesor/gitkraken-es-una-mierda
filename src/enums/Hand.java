package enums;

public enum Hand {
	Axe,
	AxeTwoHanded,
	Sword,
	Dagger,
	Hammer,
	Staff,
	Wand,
	Mace,
	BroadSword,
	Bow,
	Crossbow
	
}
