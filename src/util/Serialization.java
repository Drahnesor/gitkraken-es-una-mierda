package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization {
	static String Directory = "Serialize\\Objects.ser";
	public static void Serialize(item.Inventory Object) {
        try
        {    
            //Saving of object in a file 
            FileOutputStream file = new FileOutputStream(Directory); 
            ObjectOutputStream out = new ObjectOutputStream(file); 
            // Method for serialization of object 
            out.writeObject(Object); 
            out.close(); 
            file.close(); 
            System.out.println("Object has been serialized"); 
        } 
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
        } 
	}
	public static item.Inventory Deserialization() {
		item.Inventory object = null;
        try
        {    
            // Reading the object from a file 
            FileInputStream file = new FileInputStream(Directory); 
            ObjectInputStream in = new ObjectInputStream(file); 
            // Method for deserialization of object 
            object = (item.Inventory)in.readObject(); 
            in.close(); 
            file.close(); 
            System.out.println("Object has been deserialized ");
            return object;
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught");
            return object;
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException is caught");
            return object;
        } 
	}
	
}
