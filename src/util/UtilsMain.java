package util;

public class UtilsMain {
	static int CounterCopies;
	
	public static int GetCopy() {
		int CopyNumber = UtilsMain.CounterCopies;
		UtilsMain.CounterCopies += 1;
		return CopyNumber;
	}
	public static int ExpLvL(int ActualLevel) {
		return (int) Math.round(0.04 * (ActualLevel ^ 3) + 0.8 * (ActualLevel ^ 2) + 2 * ActualLevel);
	}
}